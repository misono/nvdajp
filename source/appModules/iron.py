#appModules/iron.py
#A part of NonVisual Desktop Access (NVDA)
#This file is covered by the GNU General Public License.
#See the file COPYING for more details.
# Copyright (C) 2012-2013 NV Access Limited

"""App module for SRWare Iron
"""

from .chrome import *
