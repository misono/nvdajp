A part of NonVisual Desktop Access (NVDA)
This file is covered by the GNU General Public License.
See the file COPYING for more details.
Copyright (C) 2013 Takuya Nishimoto

checkCharDesc.py
====================================

character description consistency check

> cd jptools
> python checkCharDesc.py


checkSymbols.py
====================================

symbols consistency check

> cd jptools
> python checkSymbols.py


updateCharDesc.py
====================================

convert characters.dic to characterDescriptions.dic

> cd jptools
> python updateCharDesc.py > newfile.dic


jtalkRunner.py
====================================

JTalk test runner.

> cd jptools
> python jtalkRunner.py


jpBrailleRunner.py
====================================

Japanese braille test harness (new version)

> cd jptools
> python jpBrailleRunner.py

Output files:

  * __h1output.txt
  * __h2output.txt
  * __h2log.txt

evaluate.py
====================================

Japanese braille test harness (legacy version)

> cd jptools
> python evaluate.py


